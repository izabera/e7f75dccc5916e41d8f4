#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

int inwhich (string tofind, vector<string> options) {
	for (unsigned int i = 0; i < options.size(); i++) if (options[i].find(tofind) != string::npos) return i;
	return -1;
}

int value (string word, vector<string> fingers, vector<string> hands, vector<string> rows) {
	unsigned int val, i;
	val = 0;
	for (i = 0; i < word.size(); i++) {
		// +1 not in home row
		// +2 two keys on the same hand
		// +2 tow keys on the same finger
		// +2  two different keys on the same finger
		if (inwhich(word.substr(i, 1), rows) != 1) val ++;
		if (i > 0) {

			if (inwhich(word.substr(i-1, 1), hands) == inwhich(word.substr(i, 1), hands)) val += 2;
			if (inwhich(word.substr(i-1, 1), fingers) == inwhich(word.substr(i, 1), fingers)) val += 2;
			if (word.substr(i-1, 1) != word.substr(i, 1) && inwhich(word.substr(i-1, 1), fingers) == inwhich(word.substr(i, 1), fingers)) val += 2;
		}
	}
	return val;
}

void printdefault() {
	cerr << "Usage: finger [-l layout] FILE" << endl;
	cerr << "if FILE is not set, reads input from stdin" << endl;
	cerr << "if -l LAYOUT is not set, us_qwerty will be used" << endl;
	cerr << "supported layouts: us_querty, ita_querty, dvorak, colemak" << endl;
	exit(1);
}

int main (int argc, char** argv) {

/*
notes:
c++11 vector syntax, requires g++ -std=c++11

us qwerty is retarded, italian version is much better
really, z with pinky? of course you get CTS

todo:
- find a better way to handle console input
- add more kbd layouts
- use a class or a struct to handle kbd layouts
- uppercase letters, numbers, symbols, whatever
- ???
- profit
*/
	vector<string> us_qwerty_fingers = {"qaz", "wsx", "edc", "rtfgcb", "yuhjnm", "ik", "ol", "p"};
	vector<string> us_qwerty_rows = {"qwertyuiop", "asdfghjkl", "zxcvbnm"};

	//regular ita_qwerty
	//vector<string> ita_qwerty_fingers = {"qa", "wsz", "edx", "rtfgcv", "yuhjbnm", "ik", "ol", "p"};

	//how i use it
	vector<string> ita_qwerty_fingers = {"qa", "wesz", "rdx", "tfgcv", "yuhjbnm", "ik", "olp", ""};
	vector<string> ita_qwerty_rows = {"qwertyuiop", "asdfghjkl", "zxcvbnm"};

	vector<string> dvorak_fingers = {"a", "oq", "ej", "pyuikx", "fgdhbm", "ctw", "rnv", "lsz"};
	vector<string> dvorak_rows = {"pyfgcrl", "aoeiudhtns", "qjkxbmwvz"};

	vector<string> colemak_fingers = {"qaz", "wrx", "fsc", "pgtdvb", "jlhnkm", "ue", "yi", "o"};
	vector<string> colemak_rows = {"qwfpgjluy", "arstdhneio", "zxcvbkm"};

	vector<string> fingers = us_qwerty_fingers, rows = us_qwerty_rows;

	string word, filename, layout;

	bool setfile = false, setlayout = false;
	vector<string> args(argv, argv+argc);
	if (argc > 1) {
		if (argc == 1) ;
		else if (argc == 2) {
			if (args[1] == "-l") printdefault();
			else {
				filename = args[1];
				setfile = true;
			}
		}
		else if (argc == 3) {
			if (args[1] != "-l") printdefault();
			else {
				layout = args[2];
				setlayout = true;
			}
		}
		else if (argc == 4) {
			if (args[1] == "-l") {
				layout = args[2];
				filename = args[3];
			}
			else if (args[2] == "-l") {
				filename = args[1];
				layout = args[3];
			}
			else printdefault();
			setfile = true;
			setlayout = true;
		}
		else printdefault();
	}

	if (setlayout) {
		if (layout == "us_qwerty") {
			fingers = us_qwerty_fingers;
			rows = us_qwerty_rows;
		}
		else if (layout == "ita_qwerty") {
			fingers = ita_qwerty_fingers;
			rows = ita_qwerty_rows;
		}
		else if (layout == "dvorak") { 
			fingers = dvorak_fingers;
			rows = dvorak_rows;
		}
		else if (layout == "colemak") {
			fingers = colemak_fingers;
			rows = colemak_rows;
		}
		else {
			cerr << "Layout " << layout << " not found, using us_qwerty instead" << endl;
			layout = "us_qwerty";
		}
	}
	else {
		cerr << "Layout not set, using us_qwerty instead" << endl;
		layout = "us_qwerty";
	}

	vector<string> hands = {fingers[0]+fingers[1]+fingers[2]+fingers[3], fingers[4]+fingers[5]+fingers[6]+fingers[7]};

	if (setfile) {
		ifstream fin (filename);
		if (!fin.good()) {
			cerr << "Could not access file " << filename << endl;
			exit(2);
		}
		while (fin >> word) cout << value(word, fingers, hands, rows) << " " << word << endl;
		fin.close();
	}
	else while (cin >> word) cout << value(word, fingers, hands, rows) << " " << word << endl;
	return 0;
}
